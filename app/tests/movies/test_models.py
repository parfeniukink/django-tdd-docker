import pytest

from movies.models import Movie


pytestmarker = pytest.mark.django_db


@pytestmarker
def test_movie_model():
    movie = Movie(title="Iron Man",
                  genre="action",
                  year="2008")
    movie.save()
    assert movie.title == "Iron Man"
    assert movie.genre == "action"
    assert movie.year == "2008"
    assert movie.created_at
    assert movie.updated_at
    assert str(movie) == movie.title


@pytestmarker
def test_add_movie_valid_json(client):
    assert Movie.objects.all().count() == 0

    response = client.post("/api/movies/",
                           {
                               "title": "The Big Lebowski",
                               "genre": "comedy",
                               "year": "1998",
                           }, content_type="application/json")

    assert response.status_code == 201
    assert response.data['title'] == "The Big Lebowski"
    assert Movie.objects.all().count() == 1


@pytestmarker
def test_add_movie_invalid_json(client):
    assert Movie.objects.all().count() == 0

    response = client.post("/api/movies/",
                           {
                               "title": "The Big Lebowski",
                               "genre": "comedy",
                           }, content_type="application/json")
    assert response.status_code == 400
    assert Movie.objects.all().count() == 0


@pytestmarker
def test_add_movie_empty_json(client):
    assert Movie.objects.all().count() == 0

    response = client.post("/api/movies/",
                           {}, content_type="application/json")
    assert response.status_code == 400
    assert Movie.objects.all().count() == 0


@pytestmarker
def test_get_single_movie(client, add_movie):
    movie = add_movie(title="The Big Lebowski",
                      genre="comedy",
                      year="1998")

    response = client.get(f"/api/movies/{movie.id}/",
                          {
                              "title": "The Big Lebowski",
                              "genre": "comedy",
                              "year": "1998",
                          }, content_type="application/json")
    assert response.status_code == 200
    assert response.data["title"] == "The Big Lebowski"


@pytestmarker
def test_get_all_movies(client, add_movie):
    movie_one = add_movie(title="The Big Lebowski",
                          genre="comedy", year="1998")
    movie_two = add_movie("No Country for Old Men", "thriller", "2007")
    resp = client.get("/api/movies/")
    assert resp.status_code == 200
    assert resp.data[0]["title"] == movie_one.title
    assert resp.data[1]["title"] == movie_two.title

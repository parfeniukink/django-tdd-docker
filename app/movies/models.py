from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


class CustomUser(AbstractUser):
    pass


class Movie(models.Model):

    title = models.CharField(_("title"), max_length=100)
    genre = models.CharField(_("genre"), max_length=50)
    year = models.CharField(_("year"), max_length=4)
    created_at = models.DateTimeField(_("created_at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated_at"), auto_now=True)

    class Meta:
        verbose_name = _("movie")
        verbose_name_plural = _("movies")

    def __str__(self):
        return self.title
